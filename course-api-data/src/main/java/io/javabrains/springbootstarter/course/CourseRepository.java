package io.javabrains.springbootstarter.course;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, String> {
  // Using 'findByProperty' name format, Spring Data JPA will implement the method for you
  public List<Course> findByTopicId(String topicId);
}
